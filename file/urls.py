from django.urls import path
from . import apis

urlpatterns = [
    path('get_storages', apis.get_storages, name='Get storages'),
    path('upload', apis.upload, name='Upload files'),
    path('remove', apis.remove, name='Remove files'),
    path('transcode', apis.transcode, name='Transcode file'),
    path('get_auth', apis.get_auth, name='get_auth file'),
]