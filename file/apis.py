from datetime import datetime
import io
from minio import Minio
from minio.error import S3Error
from utils.models import Request

from utils import constants, messages, exceptions, apis as utils_apis
from rest_framework.decorators import api_view
from django.conf import settings
from utils.service import request_apis

client = Minio(
    settings.SERVER_NAME,
    access_key=settings.BUCKET_ACCESS_KEY,
    secret_key=settings.BUCKET_SECRET_KEY,
    secure=True,
    region=settings.REGION,
)

@api_view(['GET'])
@utils_apis.query_debugger
def get_storages(request):
    '''
    Get storages
    '''
    try:
        buckets = list_buckets()
        response = utils_apis.BaseJSONResponse(data=buckets, errorCode=0, message='Success')
    except Exception as exception:
        response = utils_apis.handle_exceptions(exception)
    return response.response()

@api_view(['POST'])
@utils_apis.query_debugger
def upload(request):
    '''
    Upload files
    '''
    try:
        result = upload_files(request)
        response = utils_apis.BaseJSONResponse(data=result, errorCode=0, message='Success')
    except Exception as exception:
        response = utils_apis.handle_exceptions(exception)
    return response.response()

@api_view(['DELETE'])
@utils_apis.query_debugger
def remove(request):
    '''
    Remove file
    '''
    try:
        # Get object information.
        file_name = request.GET.get('file_name', None)
        if file_name:
            result = client.stat_object(settings.BASE_BUCKET_NAME, file_name)
            if not result:
                raise exceptions.NotFoundException(messages.FILE_NOT_FOUND)

            # Remove object.
            client.remove_object(settings.BASE_BUCKET_NAME, file_name)

            response = utils_apis.BaseJSONResponse(data=True, errorCode=0, message='Success')
        else:
            raise exceptions.InvalidArgumentException(messages.FILE_NAME_EMPTY)

    except Exception as exception:
        response = utils_apis.handle_exceptions(exception)
    return response.response()

@api_view(['GET'])
@utils_apis.query_debugger
def get_auth(request):
    '''
    get_auth
    '''
    try:
        body = request.GET.get('body', None)
        if not body:
            body = """
            {
                "ingestion": {
                    "url": "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/demo/LevelUp.mp4"
                },
                "processing": {
                    "transmux_only": true,
                    "audio_only": false,
                    "preset_groups": [
                        {
                            "id": 266
                        },
                        {
                            "id": 265
                        }
                    ],
                    "formats": [
                        {
                            "format": "HLS",
                            "two_pass": true,
                            "black_frame_removal": false,
                            "protection_method": "None",
                            "distributions": [
                                {
                                    "url": "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/transcoded/hls/"
                                }
                            ]
                        },
                        {
                            "format": "MPEG-DASH",
                            "two_pass": true,
                            "black_frame_removal":"false",
                            "protection_method": "AES",
                            "aes_protection": {
                                "resource_id": "aes_resource_id" 
                            },
                            "distributions": [
                                {
                                    "url": "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/transcoded/mpeg/" 
                                }
                            ]
                        },
                        {
                            "format": "SmoothStreaming",
                            "two_pass": false,
                            "black_frame_removal":"false",
                            "protection_method": "None",
                            "distributions": [{
                                "url": "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/transcoded/mss/" 
                            }]
                        },
                        {
                            "format": "MP4",
                            "two_pass": false,
                            "black_frame_removal":"false",
                            "fragment_mp4": true,
                            "distributions": [{
                                "url": "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/transcoded/mp4/" 
                            }]
                        }
                    ]
                },
                "notification": {
                    "url": "http://google.com" 
                }
            }
            """
        
        input_request = Request(
            method='POST',
            body=body,
        )

        headers = request_apis.get_authentication_headers(input_request)
        print('=== headers: ', headers)
        response = utils_apis.BaseJSONResponse(data=headers, errorCode=0, message='Success')
    except Exception as exception:
        response = utils_apis.handle_exceptions(exception)
    return response.response()

@api_view(['POST'])
@utils_apis.query_debugger
def transcode(request):
    '''
    Transcode files
    '''
    try:
        result = create_vod_job(request)
        response = utils_apis.BaseJSONResponse(data=result, errorCode=0, message='Success')
    except Exception as exception:
        print('=== ex type: ', type(exception), ' == ex: ', exception)
        response = utils_apis.handle_exceptions(exception)
    return response.response()

def create_vod_job(request):
    file_path = request.POST.get('file_path', None)
    save_to = request.POST.get('save_to', None)
    formats = request.POST.get('formats', None)
    
    url = '%s/%s' % (settings.BUCKET_SERVER_URL, file_path)
    distribution_url = '%s/%s' % (settings.BUCKET_SERVER_URL, save_to)
    notification_url = 'https://google.com'
    body = {
        "ingestion": {
            "url": url,
            "external_audios": [],
            "external_subtitles": []
        },
        "processing": {
            "transmux_only": True,
            "audio_only": False,
            "formats": [
                {
                    "format": "HLS",
                    "two_pass": True,
                    "black_frame_removal": False,
                    "multiplex_audio_video": True,
                    "protection_method": "None",
                    # "protection_method": "AES",
                    # "aes_protection": {
                    #     "resource_id": "aes_resource_id" 
                    # },
                    "distributions": [
                        {
                            "url": distribution_url,
                            # "name": settings.DISTRIBUTION_NAME,
                        }
                    ]
                }
            ]
        },
        # "notification": {
        #     "url": notification_url,
        # }
    }

    # body = {
    #     ingestion: {
    #         url: source + params[:source_path],
    #         external_audios: [],
    #         external_subtitles: []
    #     },
    #     processing: {
    #         transmux_only: true,
    #         audio_only: false,
    #         formats: params[:outputs].map { |type|
    #             item = {
    #                 format: type.upcase,
    #                 two_pass: true,
    #                 black_frame_removal: false,
    #                 protection_method: "None",
    #                 distributions: [
    #                     {
    #                         url: output + params[:output_path]
    #                     }
    #                 ]
    #             }

    #             if type == "hls"
    #                 item[:multiplex_audio_video] = true
    #                 item[:protection_method] = params[:encrypt_type].to_i == 2 ? "AES" : "VerimatrixDRM" if params[:encrypt_type].to_i != 3

    #                 # Protection for AES
    #                 item[:aes_protection] = {
    #                     key_rotation: false,
    #                     resource_id: params[:hls_key_rid].to_i
    #                 } if item[:protection_method] == "AES"

    #                 # Protection for DRM
    #                 item[:verimatrix_protection] = {
    #                     key_rotation: false,
    #                     resource_id: params[:verimatrix][:resource_id].to_i,
    #                     scramble_url: params[:verimatrix][:scramble_url],
    #                     vcas_url: params[:verimatrix][:vcas_url]
    #                 } if item[:protection_method] == "VerimatrixDRM"
    #             end
    #         }
    #     },
    #     notification: {
    #         url: ...
    #     }
    # }

    # body = {
    #     'ingestion': {
    #         'url': "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/demo/LevelUp.mp4",
    #         'external_audios': [],
    #         'external_subtitles': []
    #     },
    #     'processing': {
    #         'transmux_only': True,
    #         'audio_only': False,
    #         'formats': [
    #             {
    #                 'format': 'HLS',
    #                 'two_pass': True,
    #                 'black_frame_removal': False,
    #                 'multiplex_audio_video': True,
    #                 'protection_method': "None",
    #                 'distributions': [
    #                     {
    #                         'url': "ftp://hoang-files:admin0100@ftp.vncdn.vn:21/transcoded/hls/",
    #                     }
    #                 ]
    #             },
    #         ],
    #     }
    # }

    print('=== body: ', body)

    input_request = Request(
        method='POST',
        body=body,
    )

    result = request_apis.create_job(input_request)
    print('=== result: ', result)
    return result

def list_buckets():
    global client
    bucket = get_bucket()

    objects = client.list_objects(
        bucket.name, prefix="my/prefix/", recursive=True,
    )
    
    for obj in objects:
        print(obj)

    # List objects information.
    print('Bucket name: ', bucket.name)
    objects = client.list_objects(bucket.name)
    files = []
    for obj in objects:
        files.append(obj.object_name)
        print(obj.object_name)

    policy = client.get_object(bucket.name, 'pub/')
    print(policy)

    return files

def upload_files(request):
    global client
    bucket = get_bucket()
    parent_file_path = request.POST.get('parent_file_path', None)
    files = request.FILES.getlist('files', None)
    parent_file_path = '{0}/'.format(parent_file_path) if parent_file_path else ''
    for file_index in files:
        file_name = file_index.name
        data = io.BytesIO(file_index.read())
        file_size = data.getbuffer().nbytes

        file = client.put_object(bucket.name, parent_file_path + file_name, data, file_size)

        print(file)
    
    return True

def get_bucket():
    global client
    buckets = client.list_buckets()
    bucket = None
    if buckets and len(buckets) > 0:
        bucket = buckets[0]
    else:
        raise exceptions.NetworkException(messages.NETWORK_EXCEPTION)

    return bucket