class EmailPayload():
    def login_info(full_name, username, password, link):
        return """
            <br>Welcome {full_name}
            <br>
            <br>Thanks for joining with us!
            <br>Your login account info
            <br>Username: {username}
            <br>Password: {password}
            <br>Please follow this link to login in then change your password for security:
            <br> {link}
            <br>Have fun, and don't hesitate to contact us with your feedback.
        """.format(full_name=full_name, username=username, password=password, link=link)

    def change_password(full_name):
        return """
            <br>Hello {full_name}
            <br>
            <br>The password for your EV-71 account has been successfully changed.
            <br>
            <br>If you did not initiate this change, please contact your administrator immediately.
        """.format(full_name=full_name)

    def reset_password(full_name, url):
        return """
            <br>Hello {full_name}
            <br>You have requested resetting password
            <br>Click the link below for changing password
            <br>{url}
        """.format(full_name=full_name, url=url)

    def reset_password_with_password(full_name, password):
        return """
            <br>Hello {full_name}
            <br>You have requested resetting password
            <br>You should change it with your new password after logining
            <br>Here is your new password
            <br>Password: {password}
        """.format(full_name=full_name, password=password)