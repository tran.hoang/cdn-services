from django.db import models
from enum import unique

@unique
class Algorithm(models.TextChoices):
    HMAC_SHA256 = 'HMAC-SHA256'

@unique
class RequestMethod(models.TextChoices):
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'


@unique
class OutputFormat(models.TextChoices):
    HLS = 'HLS',
    MPEG_DASH = 'MPEG-DASH',
    MS_SSTR = 'SmoothStreaming',
    MP4 = 'MP4'

