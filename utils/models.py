from django.conf import settings

def KeyValue(
    code=None,
    title=None,
    type=None,
    question_type=None,
    hint=None,
    limit=None,
    format=None,
    default=None,
    options=None,
):
    return {
        'code': code,
        'title': title,
        'type': type,
        'question_type': question_type,
        'hint': hint,
        'limit': limit,
        'format': format,
        'default': default,
        'options': options,
    }

def ResponseOption(value=None, display=None):
    return {
        'value': value,
        'display': display
    }

class Request:
    method=None
    host=None
    uri=None
    params=None
    body=None
    body_type=None

    def __init__(
        self, 
        method=None, 
        host=settings.BASE_VOD_API_HOST, 
        uri=settings.BASE_VOD_API_URI, 
        params=None, 
        body=None, 
        body_type=None
    ):
        self.method = method
        self.host = host
        self.uri = uri
        self.params = params
        self.body = body
        self.body_type = body_type
    