from . import exceptions 
from . import messages
from utils import constants
from uuid import UUID
import re
class Validator():
    def is_enum(cls, item, message=messages.INVALID_ARGUMENT): 
        item = item.upper()
        if item in [v.value for v in cls.__members__.values()]:
            return cls[item]
        raise exceptions.ValidationException(message)

    def is_empty_uuid(uuid_string):
        try:
            UUID(uuid_string, version=4)
            return True
        except ValueError as exception:
            raise exceptions.ValidationException(messages.INVALID_UID)

    def is_phone_number(input):
        return re.match(constants.PHONE_PATTERN, input)

    def is_email(input):
        return re.match(constants.EMAIL_PATTERN, input)