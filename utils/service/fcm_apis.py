from utils.exceptions import NetworkException
from fcm_django.fcm import fcm_send_message

def send_message(device_token, title, message, data):
    try:
        fcm_send_message(
            registration_id=device_token,
            title=title, 
            body=message, 
            data=data,
        )
    except Exception as exception:
        print('=== FCM_error_type: ', type(exception), ' == ', exception, ' ==device_token: ', device_token)