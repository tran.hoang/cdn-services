from django.conf import settings
from django.http import response
from cdn_service.settings import BUCKET_SECRET_KEY
from utils import apis as utils_apis, constants, enums
import requests
import json

BUCKET_ACCESS_KEY=settings.BUCKET_ACCESS_KEY
BUCKET_SECRET_KEY=settings.BUCKET_SECRET_KEY

def create_job(request):
    headers = get_authentication_headers(request)
    data = request.body
    url = request.host + request.uri
    
    response = requests.post(url, headers=headers, json=data)
    return json.loads(response.text)

def get_authentication_headers(request):
    date = utils_apis.get_signing_date()
    nonce = utils_apis.generate_random_number(constants.X_SFD_NONE_LENGTH)
    request_data = parse_request_to_string(request, date, nonce)
    print('=== request_string: ', str(request_data).replace('(', '').replace(')', ''))
    signature = utils_apis.generate_signature(BUCKET_SECRET_KEY, request_data)
    token = '{0} {1}:{2}'.format(enums.Algorithm.HMAC_SHA256, BUCKET_ACCESS_KEY, signature)
    headers = {
        "Content-Type": constants.VOD_CONTENT_TYPE,
        "Authentication": token,
        "X-SFD-Date": date,
        "X-SFD-Nonce": nonce
    }

    print('=== headers: ', headers)

    return headers

def parse_request_to_string(request, date, nonce):
    return (
        str(request.method.upper()) + '\n' + 
        request.uri + '\n' + 
        # 'host:' + request.host + '\n' + 
        # 'x-sfd-date:' + 
        date + '\n' + 
        # 'x-sfd-fzone:' + constants.X_SFD_FZONE + '\n' + 
        # 'x-sfd-nonce:' + 
        nonce + '\n' + 
        # 'x-sfd-signature-version:' + constants.X_SFD_SIGNATURE_VERSION + '\n' + 
        BUCKET_ACCESS_KEY + '\n' + 
        dict_to_string(request.body)
    )

def dict_to_string(input):
    result = str(input).replace(' ', '').replace('\'', '"').replace('\n', '').replace('True', 'true').replace('False', 'false')
    return result

# def register(email, full_name, password, phone_number=None, company=None, job_function='teacher'):
#     headers = get_authentication_headers()
#     data = {
#         'email': email,
#         'name': full_name,
#         'password': password,
#         'phone_number': phone_number,
#         'company': company,
#         'job_function': job_function,
#     }

#     return requests.post(_url('user_register/'), headers=headers, data=data)

# def get_teachers(page_size, page):
#     headers = get_authorization_headers()
#     data = {
#         "page_size": page_size,
#         "page": page
#     }
    
#     response = requests.post(_url('teacher_list'), headers=headers, params=data)
#     return json.loads(response.text)

# def user_matrix(page_size, page):
#     headers = get_authorization_headers()
#     data = {
#         "page_size": page_size,
#         "page": page
#     }
    
#     response = requests.post(_url('user_matrix'), headers=headers, params=data)
#     return json.loads(response.text)

# def get_access_token():
#     body = {
#         'grant_type': GRANT_TYPE,
#         'client_id': CLIENT_ID,
#         'client_secret': CLIENT_SECRET,
#         'token_type': TOKEN_TYPE
#     }

#     return requests.post(BASE_LMS_URL + 'oauth2/access_token/', data=body)

# def get_access_token_string():
#     token = get_access_token()
#     return json.loads(token.text)['access_token']

# def _url(path):
#     return BASE_API_URL + path