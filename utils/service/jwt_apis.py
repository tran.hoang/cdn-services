from oauth2_provider.models import AccessToken, Application, RefreshToken
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from oauth2_provider.settings import oauth2_settings
from utils import enums, exceptions, messages, apis
from django.utils.timezone import now, timedelta
from rest_framework.decorators import api_view
from oauthlib.common import generate_token
from utils.validator import Validator
from user_account.models import User
import json

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        if type(user) is User:
            token['username'] = user.email
        else:
            token['username'] = user.code
            
        return token

    # def validate(self, attrs):
    #     try:
    #         request = self.context["request"]
    #     except KeyError:
    #         pass

    #     try:
    #         request_data = json.loads(request.body)
    #         if("username" in request_data and "user_id" in request_data):
    #             # default scenario in simple-jwt  
    #             pass
    #         else:
    #             # some fields were missing
    #             raise exceptions.AuthenticationException(messages.EXPIRED_SESSION)
    #     except:
    #         raise exceptions.AuthenticationException(messages.EXPIRED_SESSION)

def generate_token(user):
    '''
    # Generate token
    '''
    refresh_token = MyTokenObtainPairSerializer.get_token(user)
    return {
        'refresh_token': str(refresh_token),
        'access_token': str(refresh_token.access_token),
    }
    
@api_view(['POST'])
def generate_access_token(request):
    '''
    # Generate access token
    '''
    try:
        grant_type = request.POST.get('grant_type')
        client_id = request.POST.get('client_id')
        client_secret = request.POST.get('client_secret')

        # Validate request
        validate_request(grant_type, client_id, client_secret)
        grant_type = Validator.is_enum(enums.GrantType, grant_type, messages.GRANT_TYPE_NOT_SUPPORT)

        try:
            application = Application.objects.get(client_id=client_id, client_secret=client_secret)
            user = application.user
        except Exception as exception:
            raise exceptions.NotFoundException(messages.APPLICATION_NOT_FOUND)

        try:
            old_token = AccessToken.objects.get(user=user, application=application)
        except Exception as exception:
            # Do nothing
            pass
        else:
            old_token.delete()
            old_refresh_token = RefreshToken.objects.get(user=user, access_token=old_token)
            old_refresh_token.delete()

        # Generate tokens
        expires = now() + timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS)
        access_token = AccessToken.objects.create(
            user=user,
            scope=oauth2_settings.SCOPES,
            expires=expires,
            token=generate_token(),
            application=application
        )

        refresh_token = RefreshToken.objects.create(
            user=user,
            token=generate_token(),
            application=application,
            access_token=access_token
        )

        response = apis.BaseJSONResponse(data=get_access_token_response(access_token), errorCode=0, message='Success')
    except Exception as exception:
        response = apis.handle_exceptions(exception)
    return response.response()

def get_access_token_response(access_token):
    token = {
        'access_token': access_token.token,
        'expires_in': oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
        'token_type': 'Bearer',
        'refresh_token': access_token.refresh_token.token,
        'scope': access_token.scope
    }

    return token

def validate_request(grant_type, client_id, client_secret):
    '''
    ### Validate input data
    '''
    # Validate grant type
    errors = ''
    if not grant_type:
        errors += ';' + messages.GRANT_TYPE_EMPTY

    # Validate client id
    if not client_id:
        errors += ';' + messages.CLIENT_ID

    # Validate client secret
    if not client_secret:
        errors += ';' + messages.CLIENT_SECRET

    # Raise exception if data is invalid
    if errors:
        raise exceptions.InvalidArgumentException(errors.replace(';', '', 1))