from apscheduler.triggers.cron import CronTrigger
from utils.enums import NotificationType
from django.conf import settings
from utils import constants
from datetime import timedelta
from manage import scheduler

PER_TIME = '*/1'

def schedule_job(job, job_id, start_date, end_date=None, notification_type=NotificationType.REMIND_INJECTING_SOON):
    day, week, month, year, hour, minute = None, None, None, None, None, None
    if NotificationType.REMIND_INJECTING_SOON == notification_type:
        hour = constants.REMIND_INJECTION_HOUR
        day = PER_TIME
        end_date = start_date
        start_date = start_date - timedelta(days=constants.BEFORE_INJECTION_DAYS)

    elif NotificationType.REMIND_INJECTING_LATE == notification_type:
        hour = constants.REMIND_INJECTION_HOUR
        day = PER_TIME
        end_date = start_date + timedelta(days=constants.INJECTION_LATE_DAYS)

    elif NotificationType.REMIND_FILLING_FORM_FIRST_TIME == notification_type:
        hour = constants.GUARDIAN_REMIND_TIMES[0]
        day = PER_TIME
        end_date = start_date + timedelta(days=constants.DAIRY_DAYS)

    elif NotificationType.REMIND_FILLING_FORM_FIRST_TIME_LATE == notification_type:
        hour = constants.GUARDIAN_REMIND_TIMES[1]
        day = PER_TIME
        end_date = start_date

    elif NotificationType.REMIND_FILLING_FORM_SECOND_TIME_LATE == notification_type:
        hour = constants.COORDINATOR_REMIND_TIMES[0]
        day = PER_TIME
        end_date = start_date

    trigger = create_trigger(
        day=day,
        week=week,
        month=month,
        year=year,
        hour=hour,
        minute=minute,
        start_date=start_date,
        end_date=end_date,
    )

    add_job(job, job_id, trigger)

def create_trigger(day=None, week=None, month=None, year=None, hour=None, minute=None, start_date=None, end_date=None):
    trigger = CronTrigger(
        day=day,
        week=week,
        year=year,
        month=month,
        hour=hour,
        minute=minute,
        start_date=start_date,
        end_date=end_date,
        timezone=settings.TIME_ZONE,
    )

    return trigger

def add_job(job, job_id, trigger):
    scheduler.add_job(func=job, id=job_id, trigger=trigger)
    print(job_id)
    scheduler.print_jobs()

def remove_job(job_id):
    try:
        job = scheduler.get_job(job_id)
        if job.next_run_time is None:
            scheduler.remove_job(job_id)
    except Exception as exception:
        print('=== Job_error_type: ', type(exception), ' == ', exception, ' ==job_id: ', job_id)