# Default Error, Exception messages
INVALID_UID = 'Invalid UUID'
UID_EMPTY = 'Missing uid'
INVALID_DATE = 'Invalid date'
INVALID_ARGUMENT = 'Invalid argument'
VALIDATION_EXCEPTION = 'Validation exception'
NOT_FOUND_EXCEPTION = 'Not found exception'
NETWORK_EXCEPTION = 'Network exception'
CONTACT_ADMIN_FOR_SUPPORT = 'Contact admin for support'
NO_PERMISSION = 'You have no permission to do this'
PAGE_NOT_FOUND = 'Page not found'
PERMISSION_DENIED = 'Permission Denied'

# Token
INVALID_OR_EXPIRED_TOKEN = 'Token is invalid or expired'
MISSING_ACCESS_TOKEN = 'Missing access token'
EXPIRED_SESSION = 'Session has expired'
GRANT_TYPE_EMPTY = 'Grant type must not be empty'
CLIENT_ID = 'Client id  must not be empty'
CLIENT_SECRET = 'Client secret must not be empty'
APPLICATION_NOT_FOUND = 'Application does not exist'

# Field support
SORT_TYPE_NOT_SUPPORT = 'Sort type not supported'
FIELD_NOT_SUPPORT = 'Field not supported'
METHOD_NOT_SUPPORT = 'Method not supported'
DEVICE_TYPE_NOT_SUPPORT = 'Device type not supported'
ROLE_NAME_NOT_SUPPORT = 'Role name not supported'
GENDER_NOT_SUPPORT = 'Gender not supported'
SUBJECT_STATUS_NOT_SUPPORT = 'Subject status not supported'
GUARDIAN_TYPE_NOT_SUPPORT = 'Guardian type not supported'
FILE_TYPE_NOT_SUPPORT = 'File type not supported'
NOTIFICATION_TYPE_NOT_SUPPORT = 'Notification type not supported'

# User
FIRST_NAME_EMPTY = 'First name must not be empty'
LAST_NAME_EMPTY = 'Last name must not be empty'
EMAIL_EMPTY = 'Email must not be empty'
EMAIL_EXISTED = 'Email has been used'
USER_NOT_FOUND = 'User does not exist'
PASSWORD_EMPTY = 'Password must not be empty'
ERROR_WHILE_SENDING_EMAIL = 'There is error while sending email'
EMAIL_OR_PASSWORD_NOT_CORRECT = 'Your email or password is not correct!'
DEVICE_TYPE_EMPTY = 'Device type must not be empty'
ROLE_NAME_EMPTY = 'Role must be specific'
EMPTY_CURRENT_PASSWORD = 'Please enter your current password'
EMPTY_NEW_PASSWORD = 'Please enter your new password'
CURRENT_PASSWORD_INCORRECT = 'Current password is incorrect'
REQUIRE_LOGIN_FIRST = 'Require login first'
USERNAME_EMPTY = 'Username must not be empty'

# Forgot password
PASSWORD_TOKEN_NOT_FOUND = 'Password token does not exist'

# Access token
ACCESS_TOKEN_NOT_FOUND = 'Access token does not exist'

# File
FILE_NAME_EMPTY = 'File name must not be empty'
FILE_NOT_FOUND = 'File does not exist'
