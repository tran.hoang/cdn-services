DD_MM_YYYY = '%d-%m-%Y'
YYYY_MM_DD = '%Y-%m-%d'
YYYY_MM = '%Y-%m'

HH_MM = '%H:%M'
DD_MM_YYYY_T_HH_MM_SS_Z = '%Y%m%dT%H%M%SZ'

UNICODE = 'UTF-8'
# VOD_CONTENT_TYPE = 'application/json; charset=UTF-8'
VOD_CONTENT_TYPE = 'application/json; charset=utf-8'
X_SFD_NONE_LENGTH = 5
X_SFD_FZONE = 'SG'
X_SFD_SIGNATURE_VERSION = 2

# Regex patterns
EMAIL_PATTERN = '^[a-zA-Z0-9](([.]{1}|[_]{1})?[a-zA-Z0-9])*[@]([a-z0-9]+([.]{1}|-)?)*[a-zA-Z0-9]+[.]{1}[a-z]{2,253}$'
PHONE_PATTERN = '^[+]{0,1}[0-9]{5,13}$'

    
