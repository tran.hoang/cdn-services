import re
import os
import time
import string
import random
import hashlib
import functools
from . import constants, exceptions, messages, enums
from django.core.mail import send_mail
from django.http import JsonResponse
from django.conf import settings
from django.db import connection, reset_queries
from .validator import Validator
from django.core.exceptions import ValidationError, FieldError
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta, datetime
from .template.email_payload import EmailPayload
import calendar
import hmac
import hashlib

# Import Paginator and set default page size is 10
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
PAGE_SIZE = 10
BASE_MEDIA_HOST = settings.BASE_MEDIA_HOST

def split_input_list(str_list):
    """
    Separate out individual input string from the comma, or space separated string.

    e.g.
    in: "Lorem@ipsum.dolor, sit@amet.consectetur\nadipiscing@elit.Aenean\r convallis@at.lacus\r, ut@lacinia.Sed"
    out: ['Lorem@ipsum.dolor', 'sit@amet.consectetur', 'adipiscing@elit.Aenean', 'convallis@at.lacus', 'ut@lacinia.Sed']

    `str_list` is a string coming from an input text area
    returns a list of separated values
    """

    new_list = re.split(r'[\n\r\s,]', str_list)
    new_list = [s.strip() for s in new_list]
    new_list = [s for s in new_list if s != '']

    return new_list

def get_current_host(request):
    scheme = request.is_secure() and "https" or "http"
    return f'{scheme}://{request.get_host()}/'

def check_empty_data(data):
    if data:
        empty = False
    else:
        empty = True
    return empty

# import django JSON Response
class BaseJSONResponse:
    def __init__(self, data, errorCode, message):
        self.data = data
        self.errorCode = errorCode
        self.message = message

    def response(self):
        # response_format = JsonResponse(self.data)
        response_format = JsonResponse({
            "data": self.data,
            "errorCode": self.errorCode,
            "message": self.message
        })
        return response_format

# Calculate query time and number of query statement
def query_debugger(func):
    @functools.wraps(func)
    def inner_func(*args, **kwargs):
        reset_queries()

        start_queries = len(connection.queries)

        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()

        end_queries = len(connection.queries)

        print("Function : " + func.__name__)
        print("Number of Queries : {}".format(end_queries - start_queries))
        print("Finished in : {}".format(end - start))
        print("======================================")

        return result

    return inner_func

def authenticate_user(request, message=messages.NO_PERMISSION):
    user = request.user
    print('=== type user: ', type(user))
    if user.is_anonymous:
        raise exceptions.AuthenticationException(message)

    return user

def paginate_data(request, data):
    '''
    Function to handle pagination data.

    Params:

    data: array data.

    request: request object that contain paginate info

    page: page to show (Default is 1).

    page_size: Defaults is 10 (PAGE_SIZE=10).
    
    Return a JSON data:

    response_data = {
        "totalRows": total,
        "totalPages": total_pages,
        "currentPage": page_number,
        "content": content
    }
    '''

    page = int(request.data.get('page', 1))
    page_size = int(request.data.get('page_size', PAGE_SIZE))

    # Handle page_size = 'all'
    # page_size = 0 for get all
    if page_size == 0:
        page_size = len(data) + 1
    elif page_size < 0:
        raise exceptions.InvalidArgumentException(messages.NEGATIVE_PAGE_SIZE)

    paginator = Paginator(data, page_size)

    total_pages = paginator.num_pages

    if int(total_pages) < page:
        page_number = page
        content = []
    else:
        current_page = paginator.page(page)
        page_number = current_page.number
        content = current_page.object_list

    total = paginator.count

    response_data = {
        "totalRows": total,
        "totalPages": total_pages,
        "currentPage": page_number,
        "content": content,
        "pageSize": page_size
    }

    return response_data

def build_url(path):
    '''
    Must change this method
    '''
    if not path:
        return None
    return '{}/{}'.format(BASE_MEDIA_HOST, path)

def get_file_name(file_path):
    return file_path[(file_path.index('/') + 1):len(file_path)]

class TokenMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
    
    # @query_debugger
    def __call__(self, request):
        response = self.get_response(request)
        if response.status_code == 401:
            return BaseJSONResponse(
                data=None,
                errorCode=response.status_code,
                message=messages.EXPIRED_SESSION).response()

        if response.status_code == 403:
            return BaseJSONResponse(
                data=None,
                errorCode=response.status_code,
                message=messages.PERMISSION_DENIED).response()

        if response.status_code == 404:
            if 'Not Found' == response.reason_phrase:
                return BaseJSONResponse(
                    data=None,
                    errorCode=response.status_code,
                    message=messages.PAGE_NOT_FOUND).response()

        if response.status_code == 405:
            return BaseJSONResponse(
                data=None,
                errorCode=response.status_code,
                message=response.data['detail']).response()

        if response.status_code == 302:
            # Handle 302 status_code
            pass
        elif response.status_code != 200:      
            return BaseJSONResponse(
                data=None,
                errorCode=response.status_code,
                message=messages.CONTACT_ADMIN_FOR_SUPPORT).response()

        return response

    def process_exception(self, request, exception):
        if type(exception) is exceptions.NotFoundException:
            return BaseJSONResponse(
                    data=None,
                    errorCode=404,
                    message=str(exception)).response()

def convert_string_to_date(date_string, format = constants.YYYY_MM_DD):
    try:
        if (type(date_string) is datetime) or (type(date_string) is date) or (type(date_string) is datetime.timestamp):
            return date_string
        return datetime.strptime(date_string, format)    
    except Exception as exception:
        return None

def convert_date_to_string(date, format = constants.YYYY_MM_DD):
    try:
        return date.strftime(format)    
    except Exception as exception:
        return None

def convert_time_to_string(time, pattern = constants.HH_MM):
    try:
        return time.strftime(pattern)    
    except Exception as exception:
        return None


def handle_exceptions(exception, invalid_error_message = messages.INVALID_ARGUMENT):
    print('=== type: ', type(exception), '=== ex: ', exception)
    default_message = BaseJSONResponse(data=None, errorCode=500, message=messages.CONTACT_ADMIN_FOR_SUPPORT)
    switcher = {
        exceptions.ValidationException: BaseJSONResponse(data=None, errorCode=400, message=str(exception)),
        exceptions.InvalidArgumentException: BaseJSONResponse(data=None, errorCode=400, message=str(exception)),
        exceptions.NotFoundException: BaseJSONResponse(data=None, errorCode=404, message=str(exception)),
        exceptions.AuthenticationException: BaseJSONResponse(data=None, errorCode=401, message=str(exception)),
        ValidationError: BaseJSONResponse(data=None, errorCode=400, message=invalid_error_message),
        ValueError: BaseJSONResponse(data=None, errorCode=400, message=str(exception)),
        FieldError: BaseJSONResponse(data=None, errorCode=400, message=messages.FIELD_NOT_SUPPORT),
        exceptions.NetworkException: BaseJSONResponse(data=None, errorCode=500, message=str(exception)),
    }

    return switcher.get(type(exception), default_message)

def send_email(subject, body, to):
    '''
    Create common function to send email.

    Return success, error.
    '''
    # Try to send email
    try:
        result = send_mail(
            subject=subject,
            message=body,
            from_email=None,
            fail_silently=True,
            recipient_list=to,
            html_message=body
        )
    except Exception as exception:
        raise exceptions.NetworkException(messages.ERROR_WHILE_SENDING_EMAIL)

def generate_random_string(length = settings.PASSWORD_LENGTH):
    '''
    Create a string of random characters of specified length
    '''
    chars = [
        char for char in string.ascii_uppercase + string.digits + string.ascii_lowercase
        if char not in 'aAeEiIoOuU1l'
    ]

    return ''.join((random.choice(chars) for __ in range(length)))

def generate_random_number(length = settings.PASSWORD_LENGTH):
    '''
    Create a random number
    '''
    chars = [
        char for char in string.digits
    ]

    return ''.join((random.choice(chars) for __ in range(length)))

def get_sort_type(sort_type):
    '''
    # Get sort type for queryset
    '''
    if sort_type.casefold() == 'asc':
        return ''
    elif sort_type.casefold() == 'desc' or not sort_type:
        return '-'  
    else:
        raise exceptions.InvalidArgumentException(messages.SORT_TYPE_NOT_SUPPORT)

def validate_uid(request):
    '''
    # Validate uid
    '''
    # Validate uid
    uid = request.GET.get('uid', None)
    if not uid:
        raise exceptions.InvalidArgumentException(messages.UID_EMPTY)

    # Validate uid
    Validator.is_empty_uuid(uid)
    return uid

def get_days_in_month(start_date):
    '''
    # Get days in month
    '''
    return calendar.monthrange(start_date.year, start_date.month)[1]

def generate_license_code():
    key_times = 5
    code = ''
    while key_times > 0:
        key_times -= 1
        code += '-' + generate_random_string(5)
    return (code.replace('-', '', 1)).upper()

def get_base_directory_path():
    path = os.path.dirname(os.path.abspath(__file__))
    return path.replace("ev_71_api/utils", "")

import base64
def get_base64_encoded_file(file_path):
    file = open(file_path, "rb")
    data = base64.b64encode(file.read()).decode(constants.UNICODE)
    file.close()
    return data

def hash_resource(resource):
    """
    Hash a :class:`web_fragments.fragment.FragmentResource`.
    """
    md5 = hashlib.md5()
    md5.update(resource.read())
    result = md5.hexdigest()
    return result

def get_first_character_of_string(input):
    return input[0]

def get_all_first_character(input):
    list = input.split()
    name = ''
    for string_index in list:
        name += string_index[0]

    return name

def generate_subject_code(study):
    code = get_all_first_character(study.name)
    code_length = settings.MAX_SUBJECT_CODE_LENGTH - len(code)
    code += generate_random_string(code_length)
    return code.upper()

def validate_file_extension(file_extension):
    file_extension = file_extension[1 : : ]
    if file_extension not in settings.FILE_TYPES:
        raise exceptions.InvalidArgumentException(messages.FILE_TYPE_NOT_SUPPORT)

def validate_file(file):
    # Validate file
    if file.size > settings.FILE_MAX_SIZE:
        raise exceptions.InvalidArgumentException(messages.FILE_OVER_MAX_SIZE)

    file_name, file_extension = os.path.splitext(file.name)
    validate_file_extension(file_extension)

def generate_signature(key, params, algorithm=hashlib.sha256):
    return hmac.new(bytes(key, constants.UNICODE), bytes(params, constants.UNICODE), digestmod=algorithm).hexdigest()

def get_signing_date():
    current_time = datetime.now()
    result = convert_date_to_string(current_time, constants.DD_MM_YYYY_T_HH_MM_SS_Z)
    return result